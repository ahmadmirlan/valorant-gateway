const controller = require("../controllers/controller");

function routes(app) {
    /* GET home page. */
    app.get('/', function (req, res, next) {
        res.render('index', {title: 'Express'});
    });

    app.get('/agents', controller.getAllAgents);
    app.get('/weapons', controller.getAllWeapons);
    app.get('/maps', controller.getAllMaps);
}

module.exports = {
    routes
}
