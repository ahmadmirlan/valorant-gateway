const axios = require("axios");
const _ = require('lodash');

const getAllAgents = (req, res) => {
    axios.get('https://playvalorant.com/page-data/en-us/agents/page-data.json').then(resp => {
        const data = _.get(resp.data, 'result.data.allContentstackAgentList.nodes.[0].agent_list');
        return res.send(data);
    });
}

const getAllWeapons = (req, res) => {
    axios.get('https://playvalorant.com/page-data/en-us/arsenal/page-data.json').then(resp => {
        const data = _.get(resp.data, 'result.data.allContentstackArsenal.nodes.[0].weapons_list.weapons');
        return res.send(data);
    });
}

const getAllMaps = (req, res) => {
    axios.get('https://playvalorant.com/page-data/en-us/maps/page-data.json').then(resp => {
        const data = _.get(resp.data, 'result.pageContext.data.maps.map_list');
        return res.send(data);
    });
}

module.exports = {
    getAllAgents,
    getAllWeapons,
    getAllMaps
}
