# Valorant Gateway
#### This simple server is a gateway to fetch data from Valorant Web APIs

### Routes
* /agents
* /weapons
* /maps



### How to run

#### 1 Install Packages 
 `npm install`
#### 2 Run the Server 
 `npm run start`
#### 3 Acess the Server Locally 
`http//:localhost:3000`
